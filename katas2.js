function add(p1, p2) {
    return p1 + p2;
}
console.log(add(4, 4));

function multiply(p1, p2) {
    let contador = 0;
    let resultado = 0;
    for (contador = 0; contador < p2; contador++) {
        resultado = add(resultado, p1);   //chama a função add 
    }
    return resultado;
}
console.log(multiply(4, 4));

function power(p1, p2) {
    let contador = 0;
    let resultado = 1;
    for (contador = 0; contador < p2; contador++) {
        resultado = multiply(resultado, p1);     //chama a função multiply
    }
    return resultado;
}
console.log(power(4, 4));

function factorial(p1) {
    let contador = 1;
    let resultado = 1;
    for (contador = 1; contador <= p1; contador++) {
        resultado = multiply(contador, resultado);  //chama a função multiply
    }
    return resultado;
}
console.log(factorial(4));

function fibonacci(num) {
    if (num === 2) {            //enquanto num 2 retornar 1 no console log
        return 1;
    } else if (num === 1) {     //enquanto num 1 retornar 0 no console
        return 0;
    } else {                    // não precisa declarar condição por que fica subtendido que tudo que não está nas condições anteriores receber o return da linha 42
        return fibonacci(num - 1) + fibonacci(num - 2);
    }
}
console.log(fibonacci(2))

